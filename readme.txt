Compilation Priority:

0 - Templates -> Concrete Functions;
1 - Pointers / References -> Stack Relative Addresses;
2 - Call Arguments -> Stack Relative Addresses;
3 - Functions / Call Addresses -> Code Segment;



Assembly Arguments Access:

[rbp + k] given pointers
[rbp - k] local pointers



PileCore Axioms Idea:

...;
-> separator character

x<...>[...];
-> variable typed parameterized function pointer declaration

x<...>: ...;
(parameter deduced by default value)
-> variabale typed function pointer declaration

x[...]: ...;
(type deduced by default value)
-> variable parameterized function pointer declaration

x: ...;
(type deduced by default value)
(parameter deduced by defaut value)
-> constant function reference declaration
may be used to shrink far function access

{...};
-> constant function definition

x<...>[...]{...};
-> constant function reference definition

x(...);
-> function call

T|x<...>[...]: ...;
-> variable function template declaration

T|x<...>[...]{...};
-> constant function template declaration

T|A|x<...>[...]...;
-> function template template declaration

T|x: ...;
-> constant reference template definition
may be used to shrink far function template access
